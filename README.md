## Koll Klienstuber 461/2 Individual Senior Project

Web Page: http://dwightservers.s3-website-us-west-2.amazonaws.com/

# Welcome
This repository primarily houses the code for my individual project, along with all the files created through out planning.

# Dwight 

Dwight is a server rental system that gives users the ability to select virtual machines loaded with software of one's choosing in order 
to always have a point of access to applications without the high upfront cost of hardware.

Dwight gives users a variety of different stacks to choose from:

Developer: Laptop broke and waiting for something new? RDP into our instance preconfigured with all your tools pre installed and ready to go while you
wait for UPS to deliver the new one. Or learn to code on our simplified machines. 

Gaming: Run high end games on a cheap laptop such as a $100 Chromebook as long as they have a quick internet connection.

Creative: Run applications such as Gimp, Paint, Krita.

Mining: Mine your crypto currency on our stacks created specifically with the proccesing power in mind needed to efficiently find yourself in debt. 
 
### Vision Statement

> For everyday individuals who want to be able to run software they otherwise couldnt as well as functioning
as a cost saving tool for users who might only need inconsistent use of software and computing power to better
fit their needs. The Dwight System is a web-based server rental system that provides the ability for anyone to
work on one of many virtual machines hosted in the  AWS cloud that are preconfigured with different types of 
software for a user to take control of through the Remote Desktop Protocol. The system gives anyone the ability
to access a virtual instance of a preconfigured size and software to manage through their own computers. 
The Dwight system will have two primary functions. Firstly as a cost savings tool by giving individuals the
ability to not have to invest in large upfront costs of expensive hardware. Secondly as a tool for individuals
who arent able to run certain types of software on a computer that they currently use. Unlike current server 
rental systems and streaming services, my product gives users the ability to pick from a wide range of services 
to run for a fraction of the cost it would take to buy the hardware needed to run on their own as well as removes
the upfront expensive per user cost associated with Microsoft RDS SAL fee of $4.19 / user / month.
In addition my system is not limited to just one domain of software. Users are able to quickly create instances
to play games, develop, create, and more all without having to worry about if the hardware they currently are running can perform tolerably.


Web Page: http://dwightservers.s3-website-us-west-2.amazonaws.com/



### Learn More

If you have any questions about our company, email us at kklienstuber15@mail.wou.edu. 


# Contributing

Dwight is an open source project, so anyone can contribute. However, I will not be accepting pull requests until after June 15th, 2018. At this point, the first full release of the website will be finished
and the project opened to public contributions. If you want to help us by adding to our codebase or fixing bugs at this time, please feel free to. Once again, if you have any questions, send them kklienstuber15@mail.wou.edu.



### Getting Started

If this is your first time coding using a forked repository and pull requests to contribute to a project, I highly recommend using 
[this](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow) tutorial. It walks you through step by step instructions on everything from initially forking the repository to
making your first pull request. 


#### Git Procedure

* Always have a seperate branch for each feature you are working on.
* Tests commits before pushing updates. 
* Commit messages should provide some useful insight into what changes were made, but should be short and simple.



### Software Development Process

This project is being developed using Scrum, which is an Agile Framework. We work
in two week sprints. However, while we highly recommend using this process, you are
not required to code this way to contribute to the project.


### Tools and Packages

This section is blank for now, but contents will be added to this as the information is learned.